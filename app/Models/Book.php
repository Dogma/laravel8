<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected $table='books';
    protected $fillable=['title', 'author_id', 'pages', 'price'];

    public function relAuthor() {
        return $this->hasOne('App\Models\User', 'id', 'author_id');
    }
}
