<?php

namespace App\Providers;

use App\Services\BookService;
use App\Services\Contracts\BookServiceContract;
use Illuminate\Support\ServiceProvider;

class BookServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BookServiceContract::class, BookService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        return [BookService::class];
    }
}
