<?php

namespace App\Services;

use App\Repositories\Contracts\AuthorRepositoryContract;
use App\Repositories\Contracts\BookRepositoryContract;
use App\Services\Contracts\BookServiceContract;

class BookService implements BookServiceContract {

    private $book;
    private $author;

    public function __construct(BookRepositoryContract $book, AuthorRepositoryContract $author)
    {
        $this->book = $book;
        $this->author = $author;
    }

    public function getBooksWithAuthors() {
        $bookwithAuthor=$this->book->all();
        for ($i=0; $i < sizeof($bookwithAuthor); $i++) {
            $bookwithAuthor[$i]->author = $this->author->find($bookwithAuthor[$i]->author_id);
        };
        return $bookwithAuthor;
    }
}
