<?php

namespace App\Services\Contracts;

interface BookServiceContract {

    public function getBooksWithAuthors();
}
