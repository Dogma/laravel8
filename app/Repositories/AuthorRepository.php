<?php

namespace App\Repositories;

use App\Models\Author;
use App\Repositories\Contracts\AuthorRepositoryContract;

class AuthorRepository implements AuthorRepositoryContract {

    private $author;

    public function __construct(Author $author)
    {
        $this->author = $author;
    }

    public function all() {
        return $this->author->all();
    }

    public function find($id) {
        return $this->author->find($id);
    }
}
