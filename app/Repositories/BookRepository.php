<?php

namespace App\Repositories;

use App\Repositories\Contracts\BookRepositoryContract;
use App\Models\Book;

class BookRepository implements BookRepositoryContract {

    private $book;

    public function __construct(Book $book) {
        $this->book = $book;
    }

    public function all() {
        return $this->book->all();
    }
};
