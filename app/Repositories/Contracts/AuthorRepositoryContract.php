<?php

namespace App\Repositories\Contracts;

interface AuthorRepositoryContract {

    public function all();

    public function find($id);
}
