<?php

namespace App\Repositories\Contracts;

interface BookRepositoryContract {

    public function all();
}
