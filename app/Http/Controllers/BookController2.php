<?php

namespace App\Http\Controllers;
use App\Repositories\Contracts\BookRepositoryContract;
use App\Services\Contracts\BookServiceContract;

class BookController2 extends Controller
{
    private $bookSevice;

    public function __construct(BookServiceContract $bookSevice)
    {
        $this->bookSevice = $bookSevice;
    }

    public function index() {
        $books = $this->bookSevice->getBooksWithAuthors();
        return view('books', compact('books'));
    }
}
