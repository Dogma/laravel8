@extends('templates.template')

@section('content')
    <h1 class="text-center">Visualizar</h1>
    <div class="col-8 m-auto">
        Titule: {{$book->title}}<br>
        Páginas: {{$book->pages}}<br>
        Preço: R$ {{$book->price}}<br>
        Autor: {{$book->user->name}}<br>
        Email do autor: {{$book->user->email}}
    </div>
@endsection
