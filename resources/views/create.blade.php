@extends('templates.template')

@section('content')
    <h1 class="text-center">Cadastrar</h1>
    <div class="col-8 m-auto">
        @if(@isset($errors) && count($errors)>0)
            <div class="text-center mt-4 mb-4 p-2 alert-danger">
                @foreach ($errors->all() as $err)
                    {{$err}}
                @endforeach
            </div>
        @endif
        <form class="form-group mt-4" name="fromCad" id="formCad" method="post" action="{{url('books')}}">
            @csrf
            <input class="form-control mb-3" type="text" name="title" id="title" placeholder="Titulo" required />
            <select class="form-control mb-3" name="author_id" id="author_id" required >
                <option value="">Autor</option>
                @foreach ($authors as $author)
                <option value="{{$author->id}}">{{$author->name}}</option>
                @endforeach
            </select>
            <input class="form-control mb-3" type="text" name="pages" id="pages" placeholder="Páginas" required />
            <input class="form-control mb-3" type="text" name="price" id="price" placeholder="Preço" required />
            <input class="btn btn-primary" type="submit" value="Cadastrar" />
        </form>
    </div>
@endsection
